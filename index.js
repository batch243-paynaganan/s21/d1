// console.log("Happy Monday");

let studentNumberA = "2020-1923";
let studentNumberB = "2020-1924";
let studentNumberC = "2020-1925";
let studentNumberD = "2020-1926";
let studentNumberE = "2020-1927";

// With array, we can simply write the code above like this:

let studentNumbers = ["2020-1923", "2020-1924", "2020-1925", "2020-1926", "2020-1927"];


	// [Section] Array
	/*
		-arrays are used to store multiple related values in a single variable
		-they are declared using square brackets ([]) also known as "Array Literals"
		-commonly used to store numerous amounts of data to manipulate in order to perform a number of tasks.
		-array also provide access a number of function/methods that help achieveing specific task.
		-a method is another term for functions associated with an object/array and is used to execute statements that are relevent
		-majority of methods are used to manipulate information stored within the same object
		-arrays are also object which is another type

		Syntax:
		let/const arrayName = [elementA, elementB, elementC, ...];
	*/

// common examples of Arrays
let grades = [98.5, 94.4, 89.2, 90.1];
console.log(grades);
let computerBrands = ["Acer", "Asus", "Lenovo", "Dell", "Mac", "Samsung"];
console.log(computerBrands);

// Possible use of an Array but is not recommended.
let mixedArr = [12, 'Asus', null, undefined, {}];
console.log(mixedArr);

// Alternative ways to write array

// Create an array with values from variables:
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "New York";

let cities = [city1, city2, city3];
console.log(cities);

// [Section] length property
// The .length property allows us to get and set the total number of items in an array

console.log(typeof grades.length);
console.log(cities.length);

let blankArr = [];
console.log(blankArr.length);

let array;
console.log(array);

// length property can also be used with strings. Some array methods and properties can also be used with strings
let fullName = "John Doe";
console.log(fullName.length);

// length property on strings shows the number of characters in a string. Spaces are counted as character in strings

// length property can also set the total number of items in an array meaning we can actually delete the last item in the array or shorten the array by simply updating the length property of an array

let myTasks = [
	"drink html",
	"eat javascript",
	"inhale css",
	"bake sass"];
console.log(myTasks);

myTasks.length = myTasks.length-1;
console.log(myTasks);
console.log(myTasks[1].length);

// to delete a specific item in array we can employ array methods  (which will be shown in the next session) or an algorithm (set of code to process tasks)

// another example using decrementation
cities.length--;
console.log(cities);

console.log(fullName.length);
fullName.length=fullName.length-1;
console.log(fullName.length);
console.log(fullName);

// if you can shorten the array by setting the length of the property, you can also lengthen it by adding number ito a length property

let theBeatles = ["John", "Paul", "Ringo", "George"];
theBeatles.length = theBeatles.length + 1;
console.log(theBeatles)

theBeatles[4] = "Roda";
console.log(theBeatles);

// theBeatles += "Roda";
// console.log(theBeatles);

// [Section] Reading/acession elements of arrays
	// accessing array elements is one of the more common task that we do with an array
	// this can be done through the use of index
	// each element is associated with it's own index/number

let lakersLegends = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"];
console.log(lakersLegends[1]);
console.log(lakersLegends[4]);

// we can also save/store array elements in another variable;
let currentLaker = lakersLegends[2];
console.log(currentLaker);

// We can also reassign array values using items indices.

console.log("Array before the reassignment:");
console.log(lakersLegends);

lakersLegends[2] = "Pau Gasol";
console.log("Array of the reassignment:");
console.log(lakersLegends);

// accessing the last element of an array
// since the first element of an aray will offset the value by one allowing us to get the last element.

let lastElementIndex = lakersLegends.length-1;
console.log(lakersLegends[lastElementIndex]);

// adding items into the array without using array methods
let newArr = [];
newArr[0] = "Cloud Strife";
console.log(newArr);

newArr[newArr.length]="Tifa Lockheart";
console.log(newArr);
// you can also add at the end of the array. instead of adding it in the front to avoid the risk of replacing the first items in the array
newArr[newArr.length] = "Barrett Wallace";
console.log(newArr);

// looping over an array
// you can use a for loop to iterate over all items in an array

	for(let index=0; index<=newArr.length-1; index++){
		console.log(newArr[index]);
	}
// we are going to create a for loop to check whether the elements/numbers are divisible by 5
let numArr = [5, 12, 30, 46, 40];

	for(let index = 0; index<numArr.length-1; index++){
		if(numArr[index]%5 === 0){
			console.log(numArr[index] + " is divisible by 5.")
		}
		else{
			console.log(numArr[index] + " is not divisible by 5.")
		}
	}

// [Section] Multidimensional Arrays
	// Multidimensional arrays are useful for storing complex data structures.

	let chessBoard = [
		['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
		['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
		['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
		['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
		['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
		['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
		['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
		['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8'],
		];
	console.table(chessBoard);
	console.log(chessBoard[4][5])
	console.log(chessBoard[7][3])